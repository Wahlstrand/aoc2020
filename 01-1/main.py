#!/usr/bin/python3.9

import argparse
import pathlib


class Inputs:
    def __init__(self, numbers: list[int], target_sum: int):
        self.numbers = numbers
        self.target_sum = target_sum


def read_inputs() -> Inputs:
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', type=pathlib.Path, required=True)
    parser.add_argument('--sum', type=int, required=True)
    args = parser.parse_args()
    file_contents = open(args.file, 'r')
    return Inputs(
        [int(data) for data in file_contents.readlines()],
        args.sum)


def solve(numbers: list[int], target_sum: int) -> None:
    numbers.sort()
    for lo_idx in range(0, len(numbers) - 1):
        for hi_idx in reversed(range(0, len(numbers) - 1)):
            lo = numbers[lo_idx]
            hi = numbers[hi_idx]
            hi_lo_sum = lo + hi
            if hi_lo_sum == target_sum:
                # got our target!
                print(str(lo) + '+' + str(hi) + '=' + str(target_sum))
                print('product: ' + str(lo * hi))
                return
            elif hi_lo_sum < target_sum:
                # sum is only going to get smaller from here; break
                # and keep looking with a greater lo
                break
            else:
                # sum too large, keep looking by decrementing the hi
                # value
                continue
    # we shouldn't get here if the problem has a solution :(
    print("problem has no solution :(")


def run() -> None:
    inputs = read_inputs()
    solve(inputs.numbers, inputs.target_sum)


if __name__ == '__main__':
    run()
