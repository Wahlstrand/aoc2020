#!/usr/bin/python3.9

import argparse
import pathlib


class Inputs:
    def __init__(self, numbers: list[int], target_sum: int):
        self.numbers = numbers
        self.target_sum = target_sum


def read_inputs() -> Inputs:
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', type=pathlib.Path, required=True)
    parser.add_argument('--sum', type=int, required=True)
    args = parser.parse_args()
    file_contents = open(args.file, 'r')
    return Inputs(
        [int(data) for data in file_contents.readlines()],
        args.sum)


class Result:
    def __init__(self, solved: bool, lo: int = 0, hi: int = 0):
        self.solved = solved
        self.lo = lo
        self.hi = hi


def solve2(numbers: list[int], target_sum: int) -> Result:
    # will pre-sort this but sorting an already sorted list should be super quick
    numbers.sort()
    for lo_idx in range(0, len(numbers) - 1):
        for hi_idx in reversed(range(0, len(numbers) - 1)):
            lo = numbers[lo_idx]
            hi = numbers[hi_idx]
            hi_lo_sum = lo + hi
            if hi_lo_sum == target_sum:
                # got our target!
                return Result(True, lo, hi)
            elif hi_lo_sum < target_sum:
                # sum is only going to get smaller from here; break
                # and keep looking with a greater lo
                break
            else:
                # sum too large, keep looking by decrementing the hi
                # value
                continue
    return Result(False)


def solve3(numbers: list[int], target_sum: int) -> None:
    numbers.sort()
    for idx in range(0, len(numbers) - 1):
        val = numbers[idx]
        res = solve2(numbers, target_sum - val)
        if res.solved:
            print('solution: ' + str(val) + ', ' + str(res.lo) + ', ' + str(res.hi))
            print('product: ' + str(val * res.lo * res.hi))
            return
    raise RuntimeError('No solution!')


def run() -> None:
    inputs = read_inputs()
    solve3(inputs.numbers, inputs.target_sum)


if __name__ == '__main__':
    run()
