#!/usr/bin/python3.9

import argparse
import pathlib


class Candidate:
    def __init__(self, lo: int, hi: int, letter: str, password: str):
        self.lo = lo
        self.hi = hi
        self.letter = letter
        self.password = password

    def __repr__(self):
        return str(self.lo) + ' ' + str(self.hi) + ' ' + self.letter + ' ' + self.password


class Inputs:
    def __init__(self, candidates: list[Candidate]):
        self.candidates = candidates


def parse_line(line: str) -> Candidate:
    # should probably sanitize these inputs a bit but...
    candidate_list = line.replace('-', ' ').replace(':', '').split(' ')
    return Candidate(int(candidate_list[0]),
                     int(candidate_list[1]),
                     str(candidate_list[2]),
                     str(candidate_list[3]))


def read_inputs() -> Inputs:
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', type=pathlib.Path, required=True)
    args = parser.parse_args()
    file_contents = open(args.file, 'r')
    return Inputs([parse_line(line) for line in file_contents.readlines()])


def one_of(a: bool, b: bool) -> bool:
    return a != b


def solve(candidates: list[Candidate]) -> None:
    good = 0
    for candidate in candidates:
        lo_matches = candidate.password[candidate.lo - 1] == candidate.letter
        hi_matches = candidate.password[candidate.hi - 1] == candidate.letter
        if one_of(lo_matches, hi_matches):
            good = good + 1
    print("num good: " + str(good) + '/' + str(len(candidates)))


def run() -> None:
    inputs = read_inputs()
    solve(inputs.candidates)


if __name__ == '__main__':
    run()
